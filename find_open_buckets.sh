#!/bin/bash

#############################
# Run this tool to find buckets that are open to the public anywhere
# in your GCP organization.
#
# Enjoy!
#############################

for proj in $(gcloud projects list --format="get(projectId)"); do
    echo "[*] scraping project $proj"
    for bucket in $(gsutil ls -p "$proj"); do
        echo "    $bucket"
        ACL="$(gsutil iam get "$bucket")"

        all_users="$(echo "$ACL" | grep allUsers)"
        all_auth="$(echo "$ACL" | grep allAuthenticatedUsers)"

        if [ -z "$all_users" ]
        then
              :
        else
              echo "[!] Open to all users: $bucket"
        fi

        if [ -z "$all_auth" ]
        then
              :
        else
              echo "[!] Open to all authenticated users: $bucket"
        fi
    done
done
